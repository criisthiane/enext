$(document).ready(function () {
    var products = [{"potions":{"1":{"id":1,"name":"Aging Potion","image":"aging-potion.png","price":29.99,"effect":"Causes the drinker to advance in age","ingredients":["Red Wine","Prune Juice","Hairy Fungus","Tortoise Shell","Caterpillar","Bat Tongue"]},"2":{"id":2,"name":"Bulgeye Potion","image":"bulgeye-potion.png","price":19.99,"effect":"It affects one's eyes, causing them to swell","ingredients":["Beetle eyes","Eel eyes"]},"3":{"id":3,"name":"Dragon Tonic","image":"dragon-tonic.png","price":64.99,"effect":"A tonic used to cure sick dragons","ingredients":["Eagle Owl feathers","Peacock feathers","Giant Purple Toad warts"]},"4":{"id":4,"name":"Love Potion","image":"love-potion.png","price":29.99,"effect":"The one who drinks it falls deeply in love with the first person they see","ingredients":["Petals from a red rose","Essence of violet","Canary flight and down feathers","Newt eyes"]},"5":{"id":5,"name":"Polyjuice Potion","image":"polyjuice-potion.png","price":99.99,"effect":"Allows the drinker to assume the form of someone else","ingredients":["Lacewing flies","Leeches","Powdered bicorn horn","Knotgrass","Fluxweed","Shredded Boomslang skin"]},"6":{"id":6,"name":"Sleeping Draught","image":"sleeping-draught.png","price":14.99,"effect":"Causes the drinker to fall almost instantaneously into a deep but temporary sleep","ingredients":["Sprigs of Lavender","Measures of Standard Ingredient","Blobs of Flobberworm Mucus","Valerian sprigs"]}}}];
    populateProducts();
    

      	$(".productsDiv").click(function(){
		   	$('#myModal').modal('toggle');
			$('#myModal').modal('show');
			
			openModal($(this).attr('data-id'));
	   	})


	

	function populateProducts(){
	 	//alert(Object.keys(products[0].potions).length);
	  	for(i = 1; i <= Object.keys(products[0].potions).length; i++){	
	     	var productName = products[0].potions[i].name;
	     	var image = products[0].potions[i].image;
	     	var price = products[0].potions[i].price;
	     	var id = products[0].potions[i].id;
   			$("#products").append("<div class='col-md-4'> <img class='img-responsive post_images productsDiv' data-id='" + id + "' src='src/img/products/" + image +
   			 "' alt='" + productName + "'><div class='productDescription'><div class='productName'>" + productName + 
   			 " - </div> <span class='price'> $" + price+ "</span></div>");	    	
   		}
   }

   function openModal(productId){  	
   		var productName = products[0].potions[productId].name;
   		var image = products[0].potions[productId].image;
		var price = products[0].potions[productId].price;
		var effect = products[0].potions[productId].effect;

		//$(".modal-body .row .col-md-6").text("");
		$(".productsModalImage").attr("src", "src/img/products/" + image);
		$(".productsModalImage").attr("alt", image);
		$("#modalProductName").text(productName);
		$("#modalProductEffect").text(effect);
		$(".modalProductPrice").text("$" + price);

		$("#ingredients").empty();
		for(i=0;i<Object.keys(products[0].potions[productId].ingredients).length;i++){
			//alert(products[0].potions[productId].ingredients[i]);
			$("#ingredients").append("<p class='productIngredients'>" + products[0].potions[productId].ingredients[i] + "</p>");
		}
   }

   // This will fire when document is ready:
    $(window).resize(function() {
        // This will fire each time the window is resized:
        if($(window).width() >= 500) {
            // if larger or equal
            $('.input-group').show();
            $('.footerLinks').show();
            $('#freeShippingColMobile').hide();
            $('#freeShippingCol').show();
            $('#bagColMobile').hide();
            $('#bagCol').show();
            $('#logoImage').hide();
            $('#logo').show();
        } else {
            // if smaller
            $('.input-group').hide();
            $('.footerLinks').hide();
            $('#freeShippingColMobile').show();
            $('#freeShippingCol').hide();
            $('#bagColMobile').show();
            $('#bagCol').hide();
            $('#logoImage').show();
            $('#logo').hide();
        }
    }).resize(); // This will simulate a resize to trigger the initial run.

});